function Scene(){

  var cruise = new Cruise()

  var _init = function(){

  }

  var _update = function(scrollValue){
    cruise.update(scrollValue);
  }

  return {
    init: _init,
    update: _update
  }


}

function Cruise(){
  
  var dom = document.getElementById('cruise')

  var t = {
    x: 0,
    y: 0
  }

  var minX1 = 200
  var maxX1 = 400

  var minX2 = 600
  var maxX2 = 650

  var _update = function(scrollValue){


        
    var posX = scrollValue - minX1
    var ratio = Math.max( 0.0, Math.min( 1.0, posX /maxX1) )
    t.y = (ratio * 80)

    posX = scrollValue - minX2
    ratio = Math.max( 0.0, Math.min( 1.0, posX /maxX2) )
    t.y -= (ratio * 80)

    t.x = scrollValue


    _render(t)
      // t = a

  }

  var _render = function(m){
    tr = ""
    if (m.x != 0) tr += " translateX("+m.x+"px) "
    if (m.y != 0) tr += " translateY("+m.y+"px) "
    if (tr != "") dom.style.transform = tr
  }

  return {
    update: _update
  }

}



var app = (function() {
    
    // DOM
    var root = document.getElementById('root');
    var sceneContainer = document.getElementById('content');
    
    // Main scene
    var scene = new Scene()

    var ignite = function() {
        update();
        var modal = document.getElementById('modal');
        modal.addEventListener('click', function() {
            modal.setAttribute('class', '');
        })

    }

    // RAF UPDATE
    var update = function() {
        var wscroll  = window.scrollY;
        sceneContainer.style.transform = 'translateX(' + wscroll * -1 + 'px)'
        scene.update(wscroll);
        requestAnimationFrame(update);
    }

    return {
        'start' : ignite,
    }

})();

window.addEventListener('load', app.start(), false)